﻿using BsnSkyRouterDataSync.Controllers;
using BsnSkyRouterDataSync.Entity;
using BsnSkyRouterDataSync.Properties;
using BsnSkyRouterDataSync.Services.Salesforce;
using BsnSkyRouterDataSync.Utilities;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Threading;

namespace BsnSkyRouterDataSync
{
    public partial class Service1 : ServiceBase
    {        
        private List<Timer> _timers = new List<Timer>(1);
        SPNetReportController _iridReportController = null;
        SalesforceController _salesforceController = null;
        private string m_secretKey;
        private string m_serviceUsername;
        private int m_serviceAccountNumber;
        private string m_brazilsecretKey;
        private int m_serviceBrazilAccountNumber;
        private string _salesforceUsername;
        private string _salesforcePassword;

        public Service1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the credentials for the endpoints to instantiate the controllers
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            // Create
            m_secretKey = Settings.Default.ProdIridiumWSSecretKey;
            m_serviceUsername = Settings.Default.IridiumWSUserName;
            m_serviceAccountNumber = Settings.Default.IridiumWSProviderAccountNumber;
            m_brazilsecretKey = Settings.Default.IridiumWSBrazilSecretKey;
            m_serviceBrazilAccountNumber = Settings.Default.IridiumWSBrazilProviderAccountNumber;

            _iridReportController = new SPNetReportController(m_secretKey,
                                                  m_serviceUsername,
                                                  m_serviceAccountNumber,
                                                  m_brazilsecretKey,
                                                  m_serviceBrazilAccountNumber);

            _salesforceUsername = Settings.Default.SalesforceUsername;
            _salesforcePassword = Settings.Default.SalesforcePassword;

            _salesforceController = new SalesforceController(_salesforceUsername, _salesforcePassword);

            _timers.Add(new Timer(SyncData, null, (DateTime.UtcNow.Date.AddDays(1).AddHours(2) - DateTime.UtcNow), TimeSpan.Parse(Settings.Default.UpdateInterval)));
        }

        /// <summary>
        /// Kills the timers
        /// </summary>
        protected override void OnStop()
        {
            // Stop all the timers
            _timers.ForEach(x => x.Change(Timeout.Infinite, Timeout.Infinite));
        }

        /// <summary>
        /// Syncs data from company dbs external to SkyRouter
        /// </summary>
        /// <param name="state">The object state</param>
        private void SyncData(object state)
        {
            SyncIridiumPlans();
            SyncSalesforceAccounts();
        }

        /// <summary>
        /// Syncs iridium plans to SkyRouter
        /// </summary>
        private void SyncIridiumPlans()
        {
            try
            {
                // Get US plans
                var devicePlans = _iridReportController.GetAllPlans(false);
                // Append Brazil plans
                devicePlans.AddRange(_iridReportController.GetAllPlans(true));

                var devicesNotInSR = new List<string>();

                var counter = 0;
                var totalCount = devicePlans.Count();
                using (var entities = new SkyRouterDataEntities())
                {
                    foreach (var item in devicePlans)
                    {
                        if (item.Device == "NA" && item.SIM == "NA")
                            continue;

                        long imei;
                        if (!long.TryParse(item.Device, out imei))
                            imei = 0;

                        var sim = item.SIM;
                        var device = !string.IsNullOrEmpty(item.Device) ? item.Device : item.SIM;
                        Inventory inventory;

                        if (item.Service == "SBD")
                            inventory = entities.Inventories.Where(inv => inv.IMEI_Iridium == imei).FirstOrDefault();
                        else
                            inventory = entities.Inventories.Where(inv => inv.SIM_Serial == sim && (!inv.IMEI_Iridium.HasValue || inv.IMEI_Iridium.Value == 0)).FirstOrDefault();

                        var isActive = string.IsNullOrEmpty(item.DeactivatedDt);

                        if (inventory == null)
                        {
                            if (isActive) devicesNotInSR.Add(device);
                            counter++;
                            continue;
                        }

                        inventory.IridiumPlan = isActive ? item.Plan : "Deactivated";
                        inventory.IridiumPlanLastSync = DateTime.UtcNow;
                        inventory.IridiumAccount = (int)item.Account;
                        inventory.IridiumPlanSetBy = item.CreatedBy;
                        inventory.IridiumPlanSetDt = !string.IsNullOrEmpty(item.CreatedDt) ? (DateTime?)DateTime.Parse(item.CreatedDt) : null;
                        counter++;

                        entities.SaveChanges();
                    }

                    if (Settings.Default.SendDevicesNotFoundEmail)
                    {
                        var mailHandle = new MailHandler();
                        mailHandle.SendDevicesNotFoundInSR(devicesNotInSR);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!(ex is System.Web.Services.Protocols.SoapException))
                    LogManager.GetCurrentClassLogger().Error("Failed to sync iridium plans: " + Environment.NewLine + ex.Message);
            }
        }

        /// <summary>
        /// Syncs Salesforce account owners to SkyRouter
        /// </summary>
        private void SyncSalesforceAccounts()
        {
            try
            {
                _salesforceController.Login();

                // Get all the users
                var filteredList = _salesforceController.GetAccounts();
                var users = _salesforceController.GetUsers();

                _salesforceController.Logout();

                using (var entities = new SkyRouterDataEntities())
                {
                    usermst skyRouterRecord = null;
                    Account account;
                    string ownerName = string.Empty;

                    foreach (var i in filteredList)
                    {
                        account = i as Account;
                        ownerName = ((User)users.Where(u => u.Id == account.OwnerId).First()).Name;
                        skyRouterRecord = entities.usermsts.Where(u => (u.userTypeId == 2 || u.userTypeId == 5) && u.customer_number.Contains(account.SkyRouter_Account__c)).FirstOrDefault();

                        if (skyRouterRecord != null)
                        {
                            skyRouterRecord.SalesforceAccount = account.Name;
                            skyRouterRecord.SalesforceAccountOwner = ownerName;
                            skyRouterRecord.SalesforceLastUpdate = DateTime.UtcNow;
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                if(!(ex is System.Web.Services.Protocols.SoapException))
                    LogManager.GetCurrentClassLogger().Error("Failed to sync Salesforce accounts: " + Environment.NewLine + ex.Message);
            }
        }
    }
}
