﻿using System;

namespace BsnSkyRouterDataSync.Models
{
    public enum BsnIridiumAccount
    {
        UNKNOWN = 0,
        US = 1,
        Brazil = 2
    }

    public enum BsnContractStatus
    {
        Active,
        Deactive
    }

    public class DevicePlan
    {
        public BsnIridiumAccount Account { get; set; }
        public BsnContractStatus Status { get; set; }
        public string Service { get; set; }
        public string ContractID { get; set; }
        public string Device { get; set; }
        public string SIM { get; set; }
        public string Plan { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDt { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDt { get; set; }
        public DateTime ActivationDt { get; set; }
        public DateTime LastModifiedDt { get; set; }
        public string DeactivatedDt { get; set; }
        public string Username { get; set; }
    }

}
