﻿using BsnSkyRouterDataSync.Properties;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BsnSkyRouterDataSync.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class MailHandler
    {
        /// <summary>
        /// Creates and sends the devices not found in SkyRouter email
        /// </summary>
        /// <param name="deviceList">The device list</param>
        public void SendDevicesNotFoundInSR(List<string> deviceList)
        {
            var smtp = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            var emailList = Settings.Default.EmailList.Split(',');

            var subject = "Active devices not in SkyRouter alert";
            var body = "<br/><table>";

            foreach(var item in deviceList)
            {
                body += "<tr><td>" + item + "</td></tr>";
            }

            body += "</table>";

            foreach (var email in emailList)
            {
                SendGenericEmail(email, subject, body, smtp);
            }
        }

        /// <summary>
        /// Sends a user defined email to the customer
        /// </summary>
        /// <param name="emailAddress">The email recipient</param>
        /// <param name="subject">The subject of the email</param>        
        /// <param name="body">The body of the email</param>        
        /// <param name="smtp">The smtp settings for sending the email</param>
        /// <param name="attachments">A file attachment, if one exists</param>
        /// <param name="bccEmail">The email addresses to BCC</param>
        public static void SendGenericEmail(string emailAddress, string subject, string body, SmtpSection smtp, List<Attachment> attachments = null, string bccEmail = null)
        {
            try
            {
                // Forumlate the email
                using (MailMessage message = new MailMessage())
                {
                    message.From = (new MailAddress(smtp.From));
                    message.To.Add(emailAddress);
                    message.Priority = MailPriority.Normal;
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;

                    if (!String.IsNullOrEmpty(bccEmail))
                    {
                        message.Bcc.Add(bccEmail);
                    }

                    if (attachments != null)
                    {
                        foreach (var attachment in attachments)
                        {
                            message.Attachments.Add(attachment);
                        }
                    }

                    // Send the message
                    using (SmtpClient client = new SmtpClient(smtp.Network.Host))
                    {
                        try
                        {
                            client.Send(message);
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger().Error("Failed to sync iridium plans: " + Environment.NewLine + ex.Message);
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Failed to send email: " + Environment.NewLine + ex.Message);
            }
        }
    }
}
