﻿using BsnSkyRouterDataSync.Services.Salesforce;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsnSkyRouterDataSync.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SalesforceController
    {
        private SforceService _sfService = new SforceService();
        private string _username;
        private string _password;

        public SalesforceController(string user, string password)
        {
            _username = user;
            _password = password;
        }

        /// <summary>
        /// Log in to the salesforce instance
        /// </summary>
        /// <returns></returns>
        public bool Login()
        {
            var loginResponse = _sfService.login(_username, _password);

            if (loginResponse.sessionId == null)
                return false;
            _sfService.SessionHeaderValue = new SessionHeader() { sessionId = loginResponse.sessionId };
            _sfService.Url = loginResponse.serverUrl;

            return true;
        }

        /// <summary>
        /// Log out of the salesforce instance
        /// </summary>
        /// <returns></returns>
        public void Logout()
        {
            _sfService.logout();
        }

        /// <summary>
        /// Gets all accounts that have a SkyRouter account set
        /// </summary>
        /// <returns>The list of accounts</returns>
        public IEnumerable<sObject> GetAccounts()
        {
            try
            {
                var finalList = new List<sObject>();

                QueryResult results = new QueryResult();

                int tryCount = 1;
                string errorMessage = string.Empty;

                do
                {
                    try
                    {
                        results = _sfService.query("Select Name, OwnerId, SkyRouter_Account__c FROM Account WHERE SkyRouter_Account__c != null");
                    }
                    catch(Exception ex)
                    {
                        tryCount++;
                        errorMessage = ex.Message;
                    }
                } while (results.size == 0 && tryCount <= 3);

                // Return after query fails more that 3 times
                if (results == null || (results.size == 0 && tryCount == 3))
                {
                    LogManager.GetCurrentClassLogger().Error("Failed to query Salesforce accounts after 3 tries: " + Environment.NewLine + errorMessage);
                    return new List<sObject>();
                }

                if (results.records != null)
                    finalList.AddRange(results.records);

                tryCount = 0;
                errorMessage = string.Empty;

                while (!results.done)
                {
                    do
                    {
                        try
                        {
                            results = _sfService.queryMore(results.queryLocator);
                        }
                        catch(Exception ex)
                        {
                            tryCount++;
                            errorMessage = ex.Message;
                        }
                    } while (results.size > 0 && tryCount <= 3);

                    // Return after query fails more that 3 times
                    if (results == null || (results.size == 0 && tryCount == 3))
                    {
                        LogManager.GetCurrentClassLogger().Error("Failed to query more Salesforce accounts after 3 tries: " + Environment.NewLine + errorMessage);
                        return new List<sObject>();
                    }

                    if (results.records != null)
                        finalList.AddRange(results.records);
                }

                // Get all the accounts with the SkyRouter account field set
                return finalList.Where(i => !string.IsNullOrEmpty(((Account)i).SkyRouter_Account__c));                
            }
            catch(Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Failed to query Salesforce accounts: " + Environment.NewLine + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Gets the users associated with the Salesforce instance
        /// </summary>
        /// <returns>The list of users</returns>
        public IEnumerable<sObject> GetUsers()
        {
            try
            {
                return _sfService.queryAll("Select Id, Name FROM User ").records.ToList();
            }
            catch(Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Failed to query Salesforce users: " + Environment.NewLine + ex.Message);
                throw ex;
            }
        }
    }
}
