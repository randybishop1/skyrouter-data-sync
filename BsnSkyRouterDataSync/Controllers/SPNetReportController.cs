﻿using BsnSkyRouterDataSync.Models;
using BsnSkyRouterDataSync.Services.IWS.Report;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace BsnSkyRouterDataSync.Controllers
{
    /// <summary>
    /// Handles calls to the iridium report service
    /// </summary>
    public class SPNetReportController
    {
        iwsreport IWS_Report_Client;

        private bool _SPNetPropertiesInstantiated = false;
        private string m_secretKey;
        private string m_serviceUsername;
        private int m_serviceAccountNumber;
        private string m_brazilsecretKey;
        private int m_serviceBrazilAccountNumber;

        /// <summary>
        /// Constructor that sets the credentials for the controller
        /// </summary>
        /// <param name="secretKey">The US secret access key</param>
        /// <param name="serviceUsername">The username for the account</param>
        /// <param name="serviceAccountNumber">The account number</param>
        /// <param name="brazilSecretKey">The Brazil account secret key</param>
        /// <param name="serviceBrazilAccountNumber">The Brazil account number</param>
        public SPNetReportController(string secretKey, string serviceUsername, int serviceAccountNumber, string brazilSecretKey, int serviceBrazilAccountNumber)
        {
            m_secretKey = secretKey;
            m_serviceUsername = serviceUsername;
            m_serviceAccountNumber = serviceAccountNumber;
            m_brazilsecretKey = brazilSecretKey;
            m_serviceBrazilAccountNumber = serviceBrazilAccountNumber;
        }

        /// <summary>
        /// Sets the necessary spnet call properties 
        /// </summary>
        private void InstantiateSPNetProperties()
        {
            IWS_Report_Client = new iwsreport();
        }

        /// <summary>
        /// Gets all contracts found in iridium for the parameter account
        /// </summary>
        /// <param name="isBrazilAccount">Should the account be the Brazil or US account?</param>
        /// <returns>The list of contracts</returns>
        public List<DevicePlan> GetAllPlans(bool isBrazilAccount)
        {
            try
            {
                IWS_Report_Client = new iwsreport();

                var totalResults = new List<string>();
                var plans = new List<DevicePlan>();
                var finalList = new List<DevicePlan>();

                using (var client = IWS_Report_Client)
                {
                    var startRecord = 1;
                    var totalRecordCount = 0;

                    var request = new createOnDemandReportRequestImpl();
                    PopulateRequestAuthentParameters(ref request, "createOnDemandReport", isBrazilAccount);
                    request.onDemandReportDetails = new onDemandReportDetailsEntryImpl()
                    {
                        reportId = "32", // Subscriber Report All
                        outputSettings = new outputSettingsEntryImpl() { },
                    };
                    request.inputParameters = new inputReportParameterEntryImpl[]
                        {
                                new inputReportParameterEntryImpl() { paramId = 8, queryId = 1, value = "30000" },
                                new inputReportParameterEntryImpl() { paramId = 9, queryId = 1, value = startRecord.ToString() }
                        };
                    request.ouputColumns = new outputReportColumnsEntryImpl[]
                    {
                        new outputReportColumnsEntryImpl() { queryId = 1, columnIds = "1,4,5,7,8,9,26,32,33,34,102" }
                    };


                    createOnDemandReportResponseImpl results;

                    // Chunk the result sets together until all results have been returned
                    do
                    {
                        request.inputParameters.Where(p => p.paramId == 9).First().value = startRecord.ToString();
                        results = client.createOnDemandReport(request);

                        if (results.totalNumberOfRecords > 0)
                        {
                            totalResults.AddRange(results.report.data);
                            startRecord += results.report.data.Count();
                        }
                        totalRecordCount = results.totalNumberOfRecords;

                    } while (totalRecordCount > 0);
                }

                string[] fields;

                // Parse the data into Device Plan objects
                foreach (var record in totalResults)
                {
                    fields = record.Replace("\\", string.Empty).Replace("\"", string.Empty).Split(',');
                    plans.Add(new DevicePlan
                    {
                        Account = isBrazilAccount ? BsnIridiumAccount.Brazil : BsnIridiumAccount.US,
                        Service = fields[1],
                        ContractID = fields[2],
                        Status = fields[3] == "Active" ? BsnContractStatus.Active : BsnContractStatus.Deactive,
                        Device = fields[4],
                        SIM = fields[5],
                        Plan = fields[6],
                        CreatedDt = fields[7],
                        DeactivatedDt = fields[9],
                        CreatedBy = fields[10]
                    });
                }

                // Filter SIM records
                var simRecords = plans.Where(s => !string.IsNullOrEmpty(s.SIM) && s.Service == "TEL").GroupBy(s => s.SIM, s => s);

                // Filter to get latest contract for each SIM
                foreach (var group in simRecords)
                {
                    finalList.Add(group.OrderByDescending(g => g.ActivationDt).First());
                }

                // Filter IMEI records
                var groupedRecords = plans.Where(s => !string.IsNullOrEmpty(s.Device) && s.Service == "SBD").GroupBy(s => s.Device, s => s);

                // Filter to get latest contract for each device
                foreach (var group in groupedRecords)
                {
                    finalList.Add(group.OrderByDescending(g => g.ActivationDt).First());
                }

                return finalList;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Failed to retrieve Iridium service plans: " + Environment.NewLine + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Populates a IWS request with the required authentication parameters
        /// </summary>
        /// <param name="request">Request object that inherits from type</param>
        /// <param name="methodName">The web service action name</param>
        /// <param name="useBrazilAccount">Flag to use Brazil account instead of US account</param>
        protected void PopulateRequestAuthentParameters<TRequest>(ref TRequest request, string methodName, bool useBrazilAccount)
            where TRequest : authenticatedIwsRequestImpl
        {
            // Create the request object
            request.timestamp = DateTime.UtcNow;
            request.signature = GenerateSignature(methodName, request.timestamp, useBrazilAccount);
            request.serviceProviderAccountNumber = useBrazilAccount ? m_serviceBrazilAccountNumber.ToString() : m_serviceAccountNumber.ToString();
            request.iwsUsername = m_serviceUsername;
            request.caller = string.Empty;
            request.callerPassword = string.Empty;
        }

        /// <summary>
        /// Generates the required authentication specified by page 33 of the IWS service manual
        /// </summary>
        /// <param name="action">The action to execute</param>
        /// <param name="timestamp">The time-stamp that will be passed into a request</param>
        /// <param name="useBrazilAccount">Flag to toggle BSN IWS accounts</param>
        /// <returns>A Base64 Encoded Signature that can be used to make a request to IWS</returns>
        protected string GenerateSignature(string action, DateTime timestamp, bool useBrazilAccount)
        {
            // Concatenate the action and the iso8601 date standard.
            byte[] value = System.Text.Encoding.ASCII.GetBytes(action + timestamp.ToString("yyyy-MM-ddTHH:mm:ssZ"));
            byte[] key = System.Text.Encoding.ASCII.GetBytes(useBrazilAccount ? m_brazilsecretKey : m_secretKey);
            byte[] hashedValue;

            // Calculate the HMAC-SHA1
            using (HMACSHA1 hash = new HMACSHA1(key))
            {
                hashedValue = hash.ComputeHash(value);
            }

            // Convert it back to text
            return Convert.ToBase64String(hashedValue);
        }
    }
}
