//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BsnSkyRouterDataSync.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class usermst
    {
        public string userid { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string ShipAddress1 { get; set; }
        public string ShipAddress2 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipZipcode { get; set; }
        public string Telephone { get; set; }
        public string Extension { get; set; }
        public string installer { get; set; }
        public string AltTelephone { get; set; }
        public string AltExt { get; set; }
        public string Fax { get; set; }
        public string MobilePhone { get; set; }
        public string EMailAddress { get; set; }
        public string ReferredBy { get; set; }
        public string CreditTerms { get; set; }
        public string Notes { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string TaxClass { get; set; }
        public int userTypeId { get; set; }
        public int restricted_user { get; set; }
        public string isactive { get; set; }
        public string createdbyuser { get; set; }
        public string invoicealert { get; set; }
        public string SOS_alert { get; set; }
        public string callusagealert { get; set; }
        public string billing_contact { get; set; }
        public string billing_phone { get; set; }
        public string billing_email { get; set; }
        public string payment_type { get; set; }
        public string BillingCountry { get; set; }
        public string mailpassword { get; set; }
        public string localmailaddr { get; set; }
        public string maildir { get; set; }
        public string ShippingCountry { get; set; }
        public string companyname { get; set; }
        public string customer_number { get; set; }
        public string FreightCarrier { get; set; }
        public string AccountNumber { get; set; }
        public string billingotherstate { get; set; }
        public string shipotherstate { get; set; }
        public Nullable<int> processed { get; set; }
        public Nullable<int> delivery_location_id { get; set; }
        public string sbd_installer { get; set; }
        public string ui_version { get; set; }
        public string preferences { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Title { get; set; }
        public string EmployeeNumber { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public int Distance_Land { get; set; }
        public int Distance_Sea { get; set; }
        public int Distance_Air { get; set; }
        public int Speed_Land { get; set; }
        public int Speed_Sea { get; set; }
        public int Speed_Air { get; set; }
        public Nullable<int> trip_location_id { get; set; }
        public string AlertSound { get; set; }
        public string AAlertSound { get; set; }
        public Nullable<int> WeatherPostalId { get; set; }
        public bool HideAssetSpeed { get; set; }
        public bool HideAssetAltitude { get; set; }
        public int Distance_Mode_Unit { get; set; }
        public string LastPageVisited { get; set; }
        public Nullable<int> TicketExpire { get; set; }
        public Nullable<int> TrackRefreshInterval { get; set; }
        public int LatLongOption { get; set; }
        public int Distance_Other { get; set; }
        public int Speed_Other { get; set; }
        public int ShowAssetTags { get; set; }
        public int AssetBreadcrumbTimespan { get; set; }
        public bool GrayoutInactiveAssets { get; set; }
        public bool EmergencyEventZoom { get; set; }
        public int EventNotificationTimeout { get; set; }
        public bool ShowGeofencesOnMap { get; set; }
        public bool AssetTagsAvoidance { get; set; }
        public int Altitude { get; set; }
        public bool ReceiveSystemNotification { get; set; }
        public bool DisplayMGRS { get; set; }
        public bool ReceivePushNotifications { get; set; }
        public Nullable<int> OrgId { get; set; }
        public string LinkedAppPin { get; set; }
        public bool IsTrialAccount { get; set; }
        public bool IsLiteAccount { get; set; }
        public string SalesforceAccount { get; set; }
        public string SalesforceAccountOwner { get; set; }
        public Nullable<System.DateTime> SalesforceLastUpdate { get; set; }
    }
}
